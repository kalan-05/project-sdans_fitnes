						// Бутерброд
const headerEl = document.getElementById('header')

window.addEventListener('scroll', function () {
	const scrollPos = window.scrollY

	if (scrollPos > 100) {
		headerEl.classList.add('header_mini')
	} else {
		headerEl.classList.remove('header_mini')
	}

})

document.addEventListener('DOMContentLoaded', function() {
	document.getElementById('burger').addEventListener('click', function() {
		document.querySelector('header').classList.toggle('open')
	})
})

let swiper = new Swiper(".gallery-swiper", {
			slidesPerView: 1,
			spaceBetween: 15,
			pagination: {
				el: '.gallery-pagination',
				type: 'fraction',
			},
			navigation: {
				nextEl: '.gallery__btn_next',
				prevEl: '.gallery__btn_prev'
			},
			breakpoints: {
				640: {
					slidesPerView: 2,
					spaceBetween: 15,
				},
				768: {
					slidesPerView: 3,
					spaceBetween: 15,
				},
				1024: {
					slidesPerView: 4,
					spaceBetween: 15,
				},
			},
})
















